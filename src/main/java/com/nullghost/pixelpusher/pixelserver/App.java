package com.nullghost.pixelpusher.pixelserver;

import static spark.Spark.*;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;
import java.util.Properties;
import java.util.Random;

import com.heroicrobot.dropbit.devices.pixelpusher.Pixel;
import com.heroicrobot.dropbit.devices.pixelpusher.Strip;
import com.heroicrobot.dropbit.registry.DeviceRegistry;

class StripObserver implements Observer {
	public boolean hasStrips = false;
	public void update(Observable registry, Object updatedDevice) {
		this.hasStrips = true;
		DeviceRegistry r = (DeviceRegistry) registry;
		r.setAntiLog(true);
		r.setLogging(false);
		r.setAutoThrottle(false);
		r.startPushing();
		System.out.println("Started PixelPusher.");
	}
}

public class App implements Runnable {
	static DeviceRegistry registry;
	static StripObserver stripObserver;

	static Thread t;
	private int lightshow;
	static Random rand;

	static HashMap<Integer, String> lightshows;

	static final int MIN_COLOR = 30;
	static final int MAX_COLOR = 240;

	static int currentRed, currentGreen, currentBlue;
	static int targetRed, targetGreen, targetBlue;
	static int nextRed, nextGreen, nextBlue;

	static Color pix[];
	static Pixel[] pixels;

	static ArrayList<String> order;

	static Properties applicationProps; 

	public App(int ls) {
		this.lightshow = ls;
	}

	public static void main(String[] args) {
		registry = new DeviceRegistry();
		stripObserver = new StripObserver();
		registry.addObserver(stripObserver);

		lightshows = new HashMap<Integer, String>();
		lightshows.put(0, "Rainbow");

		//externalStaticFileLocation("C:/Users/Justin/public_html");

		get("/", (req, res) -> { 
			res.redirect("/index.html");
			return ""; 
		});
		get("/pixelpusher/status", (req, res) -> {
			return "PixelPusher is operational.";
		});
		get("/pixelpusher/listshows", (req, res) -> {
			return lightshows;
		}, new JsonTransformer());
		get("/pixelpusher/rgb/*/*/*", (req, res) -> { 
			System.out.println("Changing to solid color.");
			boolean success = solidColor(req.splat()[0], req.splat()[1], req.splat()[2]);
			if (success) return "Update success.";
			return "Update failed.";
		});
		get("/pixelpusher/show/*", (req, res) -> {
			System.out.println("Starting a show.");
			boolean success = startShow(req.splat()[0]);
			if (success) return "Update success.";
			return "Update failed.";
		});
	}

	public static boolean solidColor(String red, String green, String blue) {
		int r, g, b;
		r = g = b = -1;

		try {
			r = Integer.parseInt(red);
			g = Integer.parseInt(green);
			b = Integer.parseInt(blue);
		} catch (NumberFormatException nfe) {
			return false;
		}

		if (r > 255 || g > 255 || b > 255 || r < 0 || g < 0 || b < 0) {
			return false;
		}

		stopShow();
		for (Strip strip : registry.getStrips()) {
			for (int stripx = 0; stripx < strip.getLength(); stripx++) {
				strip.setPixel(new Color(r, g, b).getRGB(), stripx);
			}
		}
		return true;
	}

	public static boolean startShow(String show) {
		int showID = -1;
		try {
			showID = Integer.parseInt(show);
		} catch (NumberFormatException nfe) {
			return false;
		}
		if (showID < 0 || showID > 1) {
			return false;
		}

		stopShow();
		
		t = new Thread(new App(showID));
		t.start();
		return true;
	}

	public void run() {
		System.out.println("Starting Show: " + this.lightshow);
		if (this.lightshow == 0) {
			App.rainbowShow();
		} else if (this.lightshow == 1) {
			App.colorflowShow();
		}
	}

	public static void stopShow() {
		System.out.println("Stopping Show");
		if (t != null) {
			t.interrupt();
			try {
				t.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static void rainbowShow() {
		System.out.println("Starting Rainbow Show.");
		int step = 0;
		pix = new Color[240];
		pixels = new Pixel[240];
		for (int i = 0; i < 240; i++) {
			pix[i] = Color.getHSBColor(0.0f, 1.0f, 1.0f);
		}
		while (!Thread.currentThread().isInterrupted()) {
			step = (step+1)%480;
			for (Strip strip : registry.getStrips()) {
				for (int stripx = 0; stripx < strip.getLength(); stripx++) {
					int c = pix[((stripx+step)%480)/2].getRGB();
					strip.setPixel(c, stripx);
				}
			}
			pix[step/2] = Color.getHSBColor(step/480.0f, 1.0f, 1.0f);
			try {
				Thread.sleep(33);
			} catch (InterruptedException ie) {
				System.out.println("Rainbow Show thread was interrupted.");
				Thread.currentThread().interrupt();
			}
		}
	}

	public static void colorflowShow() {
		System.out.println("Starting Colorflow Show.");
		int step = 0;
		pix = new Color[240];
		pixels = new Pixel[240];
		rand = new Random();
		order = new ArrayList<String>();

		initColor();

		while (!Thread.currentThread().isInterrupted()) {
			for (Strip strip : registry.getStrips()) {
				
				for (int stripx = 0; stripx < strip.getLength(); stripx++) {
					// While this is more network efficient, it doesn't honor setAntilog
					//pixels[stripx] = new Pixel(pix[((stripx+step)%240)].getRGB());
					//strip.setPixels(pixels);
					
					strip.setPixel(pix[((stripx+step)%240)].getRGB(), stripx);
				}
			}
			step = (step+239) % 240;

			morphColor();
			pix[step] = new Color(currentRed, currentGreen, currentBlue);
			//System.out.println("At time " + System.currentTimeMillis() + " the color is " + currentRed + ", " + currentGreen + ", " + currentBlue);
			try {
				Thread.sleep(33);
			} catch (InterruptedException ie) {
				System.out.println("Colorflow Show thread was interrupted.");
				Thread.currentThread().interrupt();
			}
		}
	}

	private static void initColor() {
		currentRed = MIN_COLOR + rand.nextInt(MAX_COLOR - MIN_COLOR + 1);
		currentGreen = MIN_COLOR + rand.nextInt(MAX_COLOR - MIN_COLOR + 1);
		currentBlue = MIN_COLOR + rand.nextInt(MAX_COLOR - MIN_COLOR + 1);
		targetRed = MIN_COLOR + rand.nextInt(MAX_COLOR - MIN_COLOR + 1);
		targetGreen = MIN_COLOR + rand.nextInt(MAX_COLOR - MIN_COLOR + 1);
		targetBlue = MIN_COLOR + rand.nextInt(MAX_COLOR - MIN_COLOR + 1);
		nextRed = MIN_COLOR + rand.nextInt(MAX_COLOR - MIN_COLOR + 1);
		nextGreen = MIN_COLOR + rand.nextInt(MAX_COLOR - MIN_COLOR + 1);
		nextBlue = MIN_COLOR + rand.nextInt(MAX_COLOR - MIN_COLOR + 1);

		for (int i = 0; i < 240; i++) {
			pix[i] = new Color(currentRed, currentGreen, currentBlue);
		}

		channelShuffle();
	}

	private static void channelShuffle() {
		order.clear();
		order.add("red");
		order.add("green");
		order.add("blue");
		long seed = System.nanoTime();
		Collections.shuffle(order, new Random(seed));
		order.add("end");
	}

	private static void morphColor() {
		if ("red".equals(order.get(0))) {
			if (currentRed < targetRed) currentRed++;
			if (currentRed > targetRed) currentRed--;
			if (currentRed == targetRed) {
				order.remove(0);
				//System.out.println("Done morphing red.");
			}
		} else if ("green".equals(order.get(0))) {
			if (currentGreen < targetGreen) currentGreen++;
			if (currentGreen > targetGreen) currentGreen--;
			if (currentGreen == targetGreen) {
				order.remove(0);
				//System.out.println("Done morphing green.");
			}
		} else if ("blue".equals(order.get(0))) {
			if (currentBlue < targetBlue) currentBlue++;
			if (currentBlue > targetBlue) currentBlue--;
			if (currentBlue == targetBlue) {
				order.remove(0);
				//System.out.println("Done morphing blue.");
			}
		}
		if ("end".equals(order.get(0))) {
			nextColor();
		}
	}

	private static void nextColor() {
		targetRed = nextRed;
		targetGreen = nextGreen;
		targetBlue = nextBlue;
		nextRed = MIN_COLOR + rand.nextInt(MAX_COLOR - MIN_COLOR + 1);
		nextGreen = MIN_COLOR + rand.nextInt(MAX_COLOR - MIN_COLOR + 1);
		nextBlue = MIN_COLOR + rand.nextInt(MAX_COLOR - MIN_COLOR + 1);
		
		//System.out.println("Next color is: " + nextRed + ", " + nextGreen + ", " + nextBlue);

		channelShuffle();
	}
	
	public static void pulseShow() {
		
	}
}
